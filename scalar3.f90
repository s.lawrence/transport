program scalar3

    ! Constants
    real, parameter :: pi = 3.141592653589793238462643383279

    ! Lattice parameters
    integer :: Lx, Ly, beta
    real :: m2, lambda
    character(len=80) :: Lxstr, Lystr, betastr, m2str, lambdastr

    ! Monte Carlo parameters
    integer :: Nsamp, Nbatch
    character(len=80) :: Nsampstr, Nbatchstr
    integer, parameter :: Ntherm = 100, Nsweep = 4, Nskip = 10
    real del
    integer :: accepted, proposed

    ! Argument parsing
    if (command_argument_count() /= 7) then
        stop "arguments: Lx Ly beta m2 lambda Nsamp Nbatch"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, betastr)
    call get_command_argument(4, m2str)
    call get_command_argument(5, lambdastr)
    call get_command_argument(6, Nsampstr)
    call get_command_argument(7, Nbatchstr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (betastr,*) beta
    read (m2str,*) m2
    read (lambdastr,*) lambda
    read (Nsampstr,*) Nsamp
    read (Nbatchstr,*) Nbatch

    del = 0.5
    accepted = 0
    proposed = 0
    call main

contains

subroutine main
    real, dimension(Lx,Ly,beta) :: phi
    real, dimension(Nsamp,beta,2) :: obs
    integer :: step, samp, batch

    phi = 0.
    do step = 1,Ntherm*Nskip
        call update(phi)
        if (step > (ntherm/2)*nskip) cycle
        if (mod(step,nskip) == 0) then
            if (accepted > proposed*0.5) del = del*1.1
            if (accepted < proposed*0.2) del = del*0.9
            !write (error_unit,'(F6.2)') accepted/real(nskip*nt*nx)*100.
            accepted = 0
            proposed = 0
        end if
    end do

    do batch = 1,Nbatch
        do samp = 1,Nsamp
            do step = 1,Nskip
                call update(phi)
            end do
            call observe(phi, obs(samp,:,:))
        end do
        print *, sum(obs,1)/Nsamp
    end do
end subroutine

pure function action_near(phi, phip, x, y, t) result(S)
    real, intent(in) :: phi(Lx,Ly,beta), phip
    integer, intent(in) :: x,y,t
    real :: S
    integer :: xp, yp, tp

    S = 0.5*m2*phip*phip + lambda/4.*phip*phip*phip*phip

    if (Lx > 1) then
        xp = mod(x,Lx)+1
        S = S + 0.5 * (phip - phi(xp,y,t))**2
        xp = mod(x-2+Lx,Lx)+1
        S = S + 0.5 * (phip - phi(xp,y,t))**2
    end if

    if (Ly > 1) then
        yp = mod(y,Ly)+1
        S = S + 0.5 * (phip - phi(x,yp,t))**2
        yp = mod(y-2+Ly,Ly)+1
        S = S + 0.5 * (phip - phi(x,yp,t))**2
    end if

    if (beta > 1) then
        tp = mod(t,beta)+1
        S = S + 0.5 * (phip - phi(x,y,tp))**2
        tp = mod(t-2+beta,beta)+1
        S = S + 0.5 * (phip - phi(x,y,tp))**2
    end if
end function

subroutine update(phi)
    real, dimension(Lx,Ly,beta), intent(inout) :: phi
    integer i
    do i = 1,Nsweep
        call sweep_metropolis(phi)
    end do
    call update_swendsen_wang(phi)
end subroutine

subroutine sweep_metropolis(phi)
    real, dimension(Lx,Ly,beta), intent(inout) :: phi
    real phip
    real S, Sp, r
    integer x, y, t

    do x = 1,Lx
        do y = 1,Ly
            do t = 1,beta
                call random_normal(r, del/sqrt(real(Lx*Ly*beta)))
                phip = phi(x,y,t) + r
                S = action_near(phi, phi(x,y,t), x,y,t)
                Sp = action_near(phi, phip, x,y,t)
                call random_number(r)
                if (r < exp(S - Sp)) then
                    phi(x,y,t) = phip
                    accepted = accepted + 1
                end if
                proposed = proposed + 1
            end do
        end do
    end do
end subroutine

subroutine update_swendsen_wang(phi)
    real, dimension(Lx,Ly,beta), intent(inout) :: phi
    logical b(Lx,Ly,beta,3), d(Lx,Ly,beta)
    integer t, x, y, tp, xp, yp
    real u, gamma, p

    b = .false.
    d = .false.

    ! Construct clusters
    do x = 1,Lx
        xp = mod(x, Lx)+1
        do y = 1,Ly
            yp = mod(y, Ly)+1
            do t = 1,beta
                tp = mod(t, beta)+1
                if (phi(x,y,t)*phi(xp,y,t) > 0) then
                    call random_number(u)
                    gamma = phi(x,y,t) * phi(xp,y,t)
                    p = 1 - exp(-2*gamma)
                    if (u < p) b(x,y,t,1) = .true.
                end if
                if (phi(x,y,t)*phi(x,yp,t) > 0) then
                    call random_number(u)
                    gamma = phi(x,y,t) * phi(x,yp,t)
                    p = 1 - exp(-2*gamma)
                    if (u < p) b(x,y,t,2) = .true.
                end if
                if (phi(x,y,t)*phi(x,y,tp) > 0) then
                    call random_number(u)
                    gamma = phi(x,y,t) * phi(x,y,tp)
                    p = 1 - exp(-2*gamma)
                    if (u < p) b(x,y,t,3) = .true.
                end if
            end do
        end do
    end do

    ! Flip clusters, half the time.
    do x = 1,Lx
        do y = 1,Ly
            do t = 1,beta
                if (d(x,y,t)) cycle
                call random_number(u)
                ! Perform flood fill
                call flood_flip(b,phi,d,x,y,t,u<0.5)
            end do
        end do
    end do
end subroutine

subroutine flood_flip(b, phi, d, x0, y0, t0, flip)
    real, intent(inout) :: phi(Lx,Ly,beta)
    logical, intent(inout) :: d(Lx,Ly,beta)
    logical, intent(in) :: b(Lx,Ly,beta,3)
    integer, intent(in) :: x0, y0, t0
    logical, intent(in) :: flip
    integer queue(Lx*Ly*beta,3), qfront, qback, x, y, t, xp, yp, tp

    ! Initialize the queue
    qfront = 1
    qback = 1
    queue(1,1) = x0
    queue(1,2) = y0
    queue(1,3) = t0
    d(x0,y0,t0) = .true.
    do while (qback >= qfront)
        x = queue(qfront, 1)
        y = queue(qfront, 2)
        t = queue(qfront, 3)
        qfront = qfront + 1

        if (flip) phi(x,y,t) = -phi(x,y,t)

        xp = mod(x,Lx)+1
        yp = y
        tp = t
        if (b(x,y,t,1) .and. .not. d(xp,yp,tp)) then
            qback = qback+1
            queue(qback,:) = (/xp,yp,tp/)
            d(xp,yp,tp) = .true.
        end if

        xp = x
        yp = mod(y,Ly)+1
        tp = t
        if (b(x,y,t,2) .and. .not. d(xp,yp,tp)) then
            qback = qback+1
            queue(qback,:) = (/xp,yp,tp/)
            d(xp,yp,tp) = .true.
        end if

        xp = x
        yp = y
        tp = mod(t,beta)+1
        if (b(x,y,t,3) .and. .not. d(xp,yp,tp)) then
            qback = qback+1
            queue(qback,:) = (/xp,yp,tp/)
            d(xp,yp,tp) = .true.
        end if

        xp = mod(x+Lx-2,Lx)+1
        yp = y
        tp = t
        if (b(xp,yp,tp,1) .and. .not. d(xp,yp,tp)) then
            qback = qback+1
            queue(qback,:) = (/xp,yp,tp/)
            d(xp,yp,tp) = .true.
        end if

        xp = x
        yp = mod(y+Ly-2,Ly)+1
        tp = t
        if (b(xp,yp,tp,2) .and. .not. d(xp,yp,tp)) then
            qback = qback+1
            queue(qback,:) = (/xp,yp,tp/)
            d(xp,yp,tp) = .true.
        end if

        xp = x
        yp = y
        tp = mod(t+beta-2,beta)+1
        if (b(xp,yp,tp,3) .and. .not. d(xp,yp,tp)) then
            qback = qback+1
            queue(qback,:) = (/xp,yp,tp/)
            d(xp,yp,tp) = .true.
        end if
    end do
end subroutine

subroutine observe(phi, obs)
    real, dimension(Lx,Ly,beta), intent(in) :: phi
    real, dimension(beta,2), intent(out) :: obs
    integer x, y, t, dt, tp
    real ot, otp

    integer y_(Lx,Ly)

    obs = 0

    do x = 1,Lx
        do y = 1,Ly
            y_(x,y) = y
        end do
    end do

    do dt = 1,beta
        do t = 1,beta
            tp = mod(t+dt-1,beta) + 1
            ! k=0 phi channel (for getting the mass)
            ot = sum(phi(:,:,t))/sqrt(real(Lx*Ly))
            otp = sum(phi(:,:,tp))/sqrt(real(Lx*Ly))
            obs(dt,1) = obs(dt,1) + ot*otp/beta

            ! k=2pi/Ly phi channel
            ot = sum(cos((2.*pi/Ly) * y_) * phi(:,:,t))/sqrt(real(Lx*Ly))
            otp = sum(cos((2.*pi/Ly) * y_) * phi(:,:,tp))/sqrt(real(Lx*Ly))
            obs(dt,2) = obs(dt,2) + ot*otp/beta

            ! TODO T00

            ! TODO T01
        end do
    end do
end subroutine

! Sample two variables from the normal distribution with given mean and standard deviation. This
! function exists because, for the Box-Muller transform, it's more natural and efficient to sample
! two variables at once.
subroutine random_normals(x, y, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x, y
    real :: a, b, d, m
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_number(a)
    call random_number(b)
    a = a * 2 * pi
    b = - log(b)
    x = b*cos(a)*d + m
    y = b*sin(a)*d + m
end subroutine

! Sample from the normal distribution with given mean and standard deviation. The mean and deviation
! default to 0 and 1, respectively.
subroutine random_normal(x, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x
    real :: d, m, y
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_normals(x, y, d, m)
end subroutine

end program
