FFLAGS = -O3 -Wall
FFLAGS += -fimplicit-none
#FFLAGS = -O0 -g -Wall

all: scalar3

scalar3: scalar3.f90
	gfortran ${FFLAGS} -o $@ $<

clean:
	${RM} scalar3
