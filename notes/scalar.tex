\documentclass[letterpaper]{amsart}

\usepackage{silence}

\usepackage[compat=1.1.0]{tikz-feynman}
\pgfkeys{/tikzfeynman/warn luatex=false}
\WarningFilter*{tikz-feynman}{The key you}

\usepackage{hyperref}
\usepackage{xcolor}
\DeclareMathOperator{\Tr}{Tr}
\newcommand{\todo}{\colorbox{pink}{\textsc{Todo}}}
\begin{document}
\title{Lattice Scalar Field Theory}
\author{Scott Lawrence}
\begin{abstract}
These notes discuss various aspects of lattice scalar field theory in three spacetime dimensions. First we discuss the required mass and wavefunction renormalizations. The second section constructs expressions for the energy-momentum tensor on the lattice.
\end{abstract}
\maketitle
\tableofcontents

These notes study the lattice discretization of $\phi^4$ Euclidean scalar field theory in three dimensions. The perturbative Lagrangian density for this theory is
\begin{equation}
\mathcal L =
\frac 1 2 (\partial\phi)^2 + \frac{m^2}{2} \phi^2 + \frac{\lambda}{4!}\phi^4
\text.
\end{equation}

\section{Renormalization}

\begin{figure}
\feynmandiagram[horizontal=a to c]{
a -- [momentum'=q] b -- c,
b -- [loop, distance=1.5cm, momentum'=k] b,
};
\hfil
\feynmandiagram[horizontal=a to d]{
a -- [momentum=q] b -- c -- d,
b -- [half left,momentum=k] c,
b -- [half right,momentum'=p] c,
};
\caption{The two divergent diagrams in $(\phi^4)_3$ perturbation theory.\label{fig:divergent}}
\end{figure}

Two Feynman diagrams contribute divergences to the propagator in $(\phi^4)_3$ theory; both are shown in Figure~\ref{fig:divergent}. No divergences are present in higher-point functions; as a result, we expect only to have mass and wavefunction renormalizations required. The ``tadpole'' on the left is linearly divergent:
\begin{equation}\label{eq:tadpole}
\Sigma_1 = -\frac \lambda 2 \int \frac{d^3 k}{(2\pi)^3} \frac{1}{k^2 + m^2}
\end{equation}
while the ``sunset'' diagram on the right has a quadratic divergence, in addition to nontrivial momentum dependence:
\begin{equation}\label{eq:sunset}
\Sigma_2(q) = \frac{\lambda^2}{6}
\int \frac{d^3 k}{(2 \pi)^3} \frac{d^3 p}{(2 \pi)^3}
\frac{1}{k^2+m^2}
\frac{1}{p^2+m^2}
\frac{1}{(q-p-k)^2+m^2}
\text.
\end{equation}
These loops are evaluated, in cutoff regularization, in Appendix~\ref{app:loops}. The purpose of these notes is to understand how the mass and wavefunction renormalizations work on the lattice. This means that we wish to look at the lattice regularized loop integrals instead, which are rather messier. As compensation, we need only evaluate the divergent pieces.

Extracting the divergent pieces is made easier by differentiating with respect to the cutoff before attempting to evaluate the diagram. Ultimately we will do this in the lattice context, but first let us demonstrate the principle on the linearly divergent tadpole. Of course, we know from Appendix~\ref{app:loops} that the cutoff-regularized tadpole diagram is
\begin{equation}
\Sigma^\Lambda_1 = -\lambda \frac{\Lambda}{4\pi^2} + (\mathrm{finite})
\text.
\end{equation}
We can obtain this more easily by regularizing (\ref{eq:tadpole}) with a cutoff $\Lambda$ and then differentiating once with respect to that cutoff:
\begin{equation}
\frac{d}{d\Lambda} \left(-\frac \lambda 2\right) \int^\Lambda\! \frac{d^3k}{(2\pi)^3} \frac 1 {k^2 + m^2}
= -\frac{\lambda}{4 \pi^2} \frac{d}{d\Lambda} \int_0^\Lambda \!dk\,\frac{k^2}{k^2 + m^2}
= -\frac \lambda {4 \pi^2} \frac{\Lambda^2}{\Lambda^2 + m^2}\text.
\end{equation}
This is finite in the large-$\Lambda$ limit (indicating that there are no super-linear divergences), and yields the expected coefficient of $-\frac\lambda{4\pi^2}$. In this case, no integrals (besides the spherical shell) had to be evaluated at all.

\subsection{Lattice regularization}
The lattice regularization is inspired by the lattice action, in which spacetime is discretized. The action reads
\begin{equation}
S_{\mathrm{lattice}} =
\sum_{\langle x y\rangle} \frac{\big(\hat\phi(x) - \hat\phi(y)\big)^2}{2}
+ \sum_x \Big[\frac{\hat m^2}{2} \hat\phi(x)^2 + \frac{\hat\lambda}{4!} \hat\phi(x)^4\Big]
\text,
\end{equation}
where the first sum is taken over all pairs $(x,y)$ of adjacent lattice sites. Because a scaling of $\phi$ does not change the path integral, this is the most general possible $\phi^4$ lattice action. Nevertheless, it is often convenient to introduce an explicit lattice spacing $a$. In the free theory, $m$ has dimension $1$, $\lambda$ dimension $1$, and $\phi$ dimension $\frac 1 2$. The dimensionless lattice quantities are therefore $\hat m = a m$, $\hat \lambda = a\lambda$, and $\hat \phi = \sqrt{a} \phi$. Using these to insert the lattice spacing yields
\begin{equation}
S_a =
\sum_{\langle x y\rangle} a \frac{\big(\phi(x) - \phi(y)\big)^2}{2}
+ \sum_x a^3 \Big[\frac{m^2}{2} \phi(x)^2 + \frac{\lambda}{4!} \phi(x)^4\Big]
\text.
\end{equation}
Thus, the lattice regularization is parameterized by the lattice spacing $a$. For a fixed spacing $a$, two changes are made to Feynamn integrals. First, all momentum integrals are truncated to $[-\frac\pi a, \frac \pi a]$:
\begin{equation}
\int d^3k \longrightarrow
\int_{-\frac\pi a}^{\frac\pi a} dk_0 \, \int_{-\frac\pi a}^{\frac\pi a} dk_1 \, \int_{-\frac\pi a}^{\frac\pi a} dk_2
\text.
\end{equation}
Second, the propagator itself is modified, to become periodic in the lattice momentum. The free lattice propagator for scalars, at spacing $a$, is given by
\begin{equation}
a^2 G(k)^{-1}
=
2 (1 - \cos a k) + (am)^2
= 
4 \sin^2 \frac {ak} 2 + (am)^2
\text.
\end{equation}
Here $\sin^2 k$ is a shorthand for $\sum_{i=1}^d \sin^2 k_i$.
It is easily verified that this inverse propagator is periodic in $k$ with period $\frac {2\pi}{a}$ (and no higher frequencies, so that there are no doublers), and has the $ak \ll 1$ limit proportional to the continuum inverse propagator $k^2 + m^2$.

Now we're ready to examine the lattice divergences. Once these are understood, it will be possible to define a perturbative lattice renormalization scheme.

\subsection{Tadpole} Using the propagator and cutoff described above, the lattice-regularized tadpole diagram is
\begin{equation}
\Sigma_1^a = -\frac \lambda {16 \pi^3} \int_{-\frac \pi a}^{\frac \pi a} d^3k\, \frac{a^2}{4 \sin^2 \frac{ak}{2} + (am)^2}
\text.
\end{equation}
Based on results for cutoff regularization, we expect this to be linearly divergent. The diagram is of course independent of the external momentum, so there will be no contribution to the wavefunction renormalization.

Note first that if we work only with lattice quantities---introducing the lattice momentum $\hat k \equiv ak$ and defining the lattice tadpole $\hat\Sigma_1$ to be the correction to the lattice correlator $\langle \hat\phi\hat\phi\rangle$---then there is no divergence at all as $a \rightarrow 0$. In fact, the lattice spacing falls out of the expression entirely:
\begin{equation}
\hat\Sigma_1^a = -\frac {\hat\lambda} {16\pi^3} \int_{-\pi}^\pi d^3\hat k\,
\frac{1}{4 \sin^2 \frac{\hat k}{2} + \hat m ^2}
\text.
\end{equation}
This is entirely expected, as when the lattice action is written in terms of dimensionless couplings, the lattice spacing also doesn't appear.

The analogue of the linear divergence in cutoff regularization is seen when taking the continuum limit while holding $m$ and $\lambda$, rather than $\hat m$ and $\hat\lambda$, fixed. In the case $\lambda=0$, this is exactly the procedure that yields the true (free) continuum theory. For non-vanishing couplings, we can see that the tadpole diagram is divergent by first noting that the integrand is always non-negative, and then restricting the integral to a spherical region of $\hat k \ll 1$.

\subsection{Sunset} The lattice-regularized sunset diagram is
\begin{equation}
\Sigma_2^a(q) = \frac{\lambda^2}{8(2\pi)^6}
\int^{\frac\pi a}_{-\frac\pi a} \!d^3 k \int^{\frac\pi a}_{-\frac\pi a}\!d^3p
\;
G(p) \,G(k)\, G(k+p-q)
\text.
\end{equation}
In cutoff regularization, this diagram was logarithmically divergent.

\subsection{Resummation}
\todo

\subsection{Running} \todo

\section{Lattice perturbation theory}
In this section we perform various perturbative computations (in lattice regularization) which can be used to check that the Monte Carlo is performing as desired, and also guide the choice of lattice parameters.

\subsection{Mass}
\todo

\section{Energy-Momentum Tensor}

\subsection{Hamiltonian density}

The Hamiltonian density of scalar field theory, in the continuum, is
\begin{equation}
H(x) = \frac 1 2 \pi(x)^2 + \frac 1 2 |\partial \phi(x)|^2 + \frac {m^2}{2} \phi(x)^2 + \frac{\lambda}{4!} \phi(x)^4
\text.
\end{equation}
When discretized onto a (Hamiltonian) lattice, this can be split into two terms. One, which we label $H_\phi$, commutes with the field operators at each site. The other, $H_\pi$, commutes with the conjugate momenta instead. The former is trivial in terms of Euclidean lattice variables, so only the latter needs discussion.

Nothing is lost by considering the one-site model, where $H_p = \frac 1 2 p^2$. The one-site Hamiltonian is
\begin{equation}
H_1 = \frac 1 2 p^2 + V(x)
\text.
\end{equation}
To derive the path integral, we decompose the partition function $\Tr e^{-\beta H_1}$ via Suzuki-Trotter, using an imaginary-time step of $1$. A single matrix element \emph{without} operator insertions, using an improved Trotterization, is
\begin{equation}
\langle x' | e^{-V(\hat x)/2} e^{-\hat p^2 / 2} e^{-V(\hat x)/2}| x\rangle
= e^{-\frac{V(x) + V(x')}{2}} \int d p\, e^{-p^2/2} e^{i p (x'-x)}
\text.
\end{equation}
Of course, with the operator $\frac 1 2\hat p^2$ inserted, the matrix element reads
\begin{equation}
\big\langle x' \big| e^{-V(\hat x)/2} e^{-\hat p^2 / 2} \frac{\hat p^2}{2} e^{-V(\hat x)/2}\big| x\big\rangle
= e^{-\frac{V(x) + V(x')}{2}} \int d p\, \frac{p^2}{2} e^{-p^2/2} e^{i p (x'-x)}
\text.
\end{equation}
In the context of a path integral, the expectation value $\langle \frac{\hat p^2}{2}\rangle$ is transformed into the expectation of a function on the space of fields, given by the ratio of the two above matrix elements:
\begin{equation}
\Big\langle \frac{\hat p^2}{2} \Big\rangle
\rightarrow
\frac{1 -(x'-x)^2}{2}
\text.
\end{equation}
Returning to field theory, the function in the path integral formalism that corresponds to the Hamiltonian density operator $H(x)$ is
\begin{equation}\label{eq:T00}
H[\phi](x)
=
V(\phi(x)) + \frac{1 - (\phi(x + \hat t) - \phi(x))^2}{2}
\text.
\end{equation}
This is, of course, equivalent to the component $\hat T^{00}$ of the energy-momentum tensor.

\subsection{Momentum density}

Peskin gives the momentum operator in the $i$th direction of continuum scalar field theory as
\begin{equation}
\hat P^i = -\int \! dx\, \hat \pi(x) \partial_i \hat \phi(x)
\text.
\end{equation}
To gain a \emph{euclidean} lattice expression, we first discretize this operator on a \emph{hamiltonian} lattice, and from there perform a Trotter-Suzuki expansion as above. To begin, the momentum density at lattice site $x$ is approximated by
\begin{equation}
\hat p^i(x) = \hat\pi(x) \Big[\hat\phi(x-\hat i) - \hat\phi(x+\hat i)\Big]
\text.
\end{equation}
Passing to the path-integral formalism just as in the previous section, we obtain
\begin{equation}
T^{0i}[\phi](x)
=
i \Big[\phi(x-\hat i) - \phi(x+\hat i) \Big]\Big[\phi(x + \hat t)  - \phi(x)\Big]
\text.
\end{equation}

\appendix
\section{Miscellaneous integrals}
\subsection{For path integrals}
Beginning with the Gaussian integral
\begin{equation}
\int_{\infty}^\infty e^{- a x^2 + bx + c}
= \sqrt{\frac{\pi}{a}} e^{\frac{b^2}{4a} + c}
\text,
\end{equation}
we can differentiate with respect to $a$ to discover
\begin{equation}
\int_{\infty}^\infty x^2 e^{- a x^2 + bx + c}
=
- \frac{d}{da}
\int_{\infty}^\infty e^{- a x^2 + bx + c}
=
\sqrt{\frac{\pi}{a}}
e^{\frac{b^2}{4a} + c}
\left[
\frac{1}{2a}
+
\frac{b^2}{4 a^2}
\right]
\text.
\end{equation}
The same result can be obtained by differentiating twice with respect to $b$. Equivalently, but slightly more conveniently, the expectation value of $x^2$ with respect to the general Gaussian is
\begin{equation}
\frac{\int_{-\infty}^\infty x^2 e^{- a x^2 + bx + c}}{\int_{-\infty}^\infty e^{- a x^2 + bx + c}}
= \frac 1 {2a} + \frac{b^2}{4 a^2}
\text.
\end{equation}

\subsection{Loops}\label{app:loops}
First let us evaluate the divergent one-loop diagram on the left of Figure~\ref{fig:divergent}. In cutoff regularization, we have
\begin{equation}
\Sigma_1^\Lambda
= -\frac{\lambda}{2}
\int^\Lambda \frac{d^3 k}{(2 \pi)^3} \frac{1}{k^2 + m^2}
=
-\frac{\lambda}{4 \pi^2} \int_0^\Lambda dk \frac{k^2}{k^2 + m^2}
\text.
\end{equation}
Rewriting the integrand as $1 - \frac{m^2}{k^2 + m^2}$, we see that the linearly divergent term is $-\frac{\lambda \Lambda}{4\pi^2}$, and the finite piece integrates to $\frac{m \lambda}{8 \pi}$. Thus we obtain the regularized loop integral
\begin{equation}
\Sigma_1^\Lambda
= -\lambda \left[\frac{\Lambda}{4 \pi^2} - \frac{m}{8 \pi}\right]
\text.
\end{equation}
Working with the lattice regulator is more difficult, but the divergence structure does not change, as seen above.

Now we turn to the divergent ``sunset'' diagram on the right of Figure~\ref{fig:divergent}:
\begin{equation}
\Sigma_2^\Lambda(q)
= \frac{\lambda^2}{6}\int^\Lambda \!\frac{d^3 k}{(2\pi)^3} \int^\Lambda \!\frac{d^3 p}{(2\pi)^3}
\frac{1}{p^2 + m^2} \frac{1}{k^2+m^2} \frac{1}{(q-p-k)^2 + m^2}
\text.
\end{equation}
\todo
\end{document}
